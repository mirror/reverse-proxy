# fsfe-reverse proxy

This is the reverse proxy which exposes FSFE services and handles SSL
termination (including Docker services).

## How to use this?

To be included in the reverse proxy, a Docker container should be started with
the environment variable `VIRTUAL_HOST`, for instance:

```bash
docker run -e "VIRTUAL_HOST=service.fsfe.org" ...
```

### Let's Encrypt

To expose the service through HTTPS, use the following env variables when
running you docker container:

```
LETSENCRYPT_HOST: <domain>
LETSENCRYPt_emaIL: <email>
```

Please make sure that <domain> resolves to the public IP address of the server
where the reverse proxy is.

### Per virtual host configuration

If you want to customize the nginx configuration for a virtual host, add or
modify [`rp/extra-conf/domain.org`](rp/extra-conf/).

### External Services

Services which run externally to the Docker host can be included in the
proxy by including a relevant configuration for them in the sites-enabled/
directory. Please note that any configuration files *must* match `*.conf`

## How does it work?

The reverse proxy automatically adds SSL certificates (via LetsEncrypt) and
sets up the reverse proxy for any Docker containers running locally.

It does that by monitoring container creation and deletion and updating its
configuration accordingly. Here is an example of the generated configuration
for our [discourse installation](https://community.fsfe.org/):

```
upstream community.fsfe.org {
    server xxx.xxx.xxx.xxx;
}
server {
    server_name community.fsfe.org;
    listen 80 ;
    access_log /var/log/nginx/access.log vhost;
    return 301 https://$host$request_uri;
}
server {
    server_name community.fsfe.org;
    listen 443 ssl http2 ;
    include /etc/nginx/vhost.d/default;
    location / {
        proxy_pass http://community.fsfe.org;
    }
}
```

## Building and deploying the reverse proxy

The reverse proxy itself is composed of three docker containers.

### rp

In the [rp](rp) folder, there is the Dockerfile used to build the nginx-based
reverse proxy. It is build on top of [that
image](https://github.com/jwilder/nginx-proxy).

There are three modifications:

* it includes the `site-enabled` folder in the nginx configuration, so it's easy
to use this reverse proxy with non-dockerised services.

* The [rp/nginx.tmpl](rp/nginx.tmpl) file (original version
  [here](https://github.com/jwilder/nginx-proxy/blob/936e57a6de5a3e12043ab51492bf795537afe4f2/nginx.tmpl))
  has been modified to use a redirect instead of a 503 HTTP error in case nginx
  can't find the requested virtual host.

* The [rp/nginx.tmpl](rp/nginx.tmpl) file (original version
  [here](https://github.com/jwilder/nginx-proxy/blob/936e57a6de5a3e12043ab51492bf795537afe4f2/nginx.tmpl))
  has been modified to use a redirect if a container is down (and has the
  `VIRTUAL_HOST` environment variable).

### rple

The rple container is the let's encrypt companion. It is responsible for issuing
and managing the TLS certificates used by the reverse proxy. More info in the
[official repository](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion).

## rp-errorpage

The [errorpage](errorpage) folder contains a simple container that displays an
error page if the reverse proxy can't find the requested `VIRTUAL_HOST`.

### Deployment with the ansible playbook

The [ansible playbook](playbook.yml) builds the two docker images mentioned
above, and run the containers with the right parameters. Here is a list of the
non-self-explanatory ones:

- The `labels` `com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy`
  is needed by the rple container so it knows for which nginx container it
  should manage certificates.
- `volumnes_from` tell docker to use the same volumes for both containers. We
  still need to specify the `volumes` instruction for both containers because
  they don't have the same rights over the volumes.
